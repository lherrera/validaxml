unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TMainForm = class(TForm)
    lblFilename: TLabel;
    lblBlurb: TLabel;
    editFilename: TEdit;
    btnBrowse: TButton;
    btnValidate: TButton;
    btnClose: TButton;
    OpenXMLDialog: TOpenDialog;
    procedure btnValidateClick(Sender: TObject);
    procedure btnBrowseClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
  end;

var
  MainForm: TMainForm;

implementation

uses msxmldom, msxml;

{$R *.dfm}

procedure TMainForm.btnBrowseClick(Sender: TObject);
begin
  if OpenXMLDialog.Execute(Application.Handle) then
    editFilename.Text := OpenXMLDialog.FileName;
end;

procedure TMainForm.btnValidateClick(Sender: TObject);
var
  xml: IXMLDOMDocument;
begin
  xml := CreateDOMDocument;
  xml.resolveExternals := true;
  xml.validateOnParse := true;

  xml.load(editFilename.Text);
  if xml.parseError.errorCode <> 0 then
    ShowMessage(xml.parseError.reason)
  else
    ShowMessage('Documento validado correctamente');
end;

procedure TMainForm.btnCloseClick(Sender: TObject);
begin
  Close;
end;

end.
