object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'Validaci'#243'n de Archivo XML'
  ClientHeight = 143
  ClientWidth = 505
  Color = clBtnFace
  Constraints.MaxHeight = 182
  Constraints.MaxWidth = 1024
  Constraints.MinHeight = 182
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    505
    143)
  PixelsPerInch = 96
  TextHeight = 17
  object lblFilename: TLabel
    Left = 20
    Top = 70
    Width = 46
    Height = 17
    Caption = 'Archivo:'
    FocusControl = editFilename
  end
  object lblBlurb: TLabel
    Left = 8
    Top = 8
    Width = 459
    Height = 34
    Caption = 
      'Para validar un documento XML contra un determinado esquema, pon' +
      'er todos los archivos (XML, XSD) en la misma carpeta.'
    WordWrap = True
  end
  object editFilename: TEdit
    Left = 72
    Top = 67
    Width = 344
    Height = 25
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 0
  end
  object btnBrowse: TButton
    Left = 422
    Top = 67
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Examinar...'
    TabOrder = 1
    OnClick = btnBrowseClick
  end
  object btnValidate: TButton
    Left = 341
    Top = 112
    Width = 75
    Height = 25
    Anchors = [akRight]
    Caption = 'Validar'
    TabOrder = 2
    OnClick = btnValidateClick
    ExplicitTop = 122
  end
  object btnClose: TButton
    Left = 422
    Top = 112
    Width = 75
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Cerrar'
    ModalResult = 2
    TabOrder = 3
    OnClick = btnCloseClick
  end
  object OpenXMLDialog: TOpenDialog
    DefaultExt = '*.xml'
    Filter = 'Archivos XML|*.xml|Todos los archivos|*.*'
    Title = 'Abrir documento XML para validar'
    Left = 24
    Top = 96
  end
end
